package de.schegge.collection.dat;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides a abstract base implementation of the {@link Trie} interface for double array tries.
 */
public abstract class AbstractDoubleArrayTrie<D extends DoubleNode> extends AbstractSet<String> implements Trie {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDoubleArrayTrie.class);

  protected DoubleArray<D> da;
  protected Alphabet alphabet;
  protected int size;

  protected AbstractDoubleArrayTrie(Alphabet alphabet, Supplier<D> supplier) {
    da = new DoubleArray<>(supplier);
    da.setBase(1, 1);
    this.alphabet = alphabet;
  }

  protected AbstractDoubleArrayTrie(DoubleArray<D> da, Alphabet alphabet, int size) {
    this.da = da;
    this.alphabet = alphabet;
    this.size = size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean add(String value) {
    String word = alphabet.checkEot(value);
    int s = 1;
    for (int i = 0; i < word.length(); i++) {
      int code = alphabet.code(word.charAt(i));
      int baseS = da.getBase(s);
      int t = baseS + code;
      int checkT = da.getCheck(t);
      if (checkT == 0) {
        String tail = word.substring(i + 1);
        da.setCheck(t, s);
        da.setTail(t, tail);
        size++;
        return true;
      }
      if (checkT != s) {
        collisionInArray(word, s, i, code, baseS, t);
        size++;
        return true;
      }
      String tail1 = da.getTail(t);
      if (tail1 != null) {
        String tail = word.substring(i + 1);
        if (tail.equals(tail1)) {
          return false;
        }
        collisionInTail(t, tail);
        size++;
        return true;
      }
      s = t;
    }
    return false;
  }

  private void collisionInArray(String word, int s, int i, int code, int baseS, int t) {
    List<Integer> list3 = da.getArcs(s, alphabet.size());
    List<Integer> list1 = da.getArcs(da.getCheck(t), alphabet.size());
    if (list3.size() + 1 >= list1.size()) {
      int q = da.xCheck(list1);
      int tempBase = da.getCheck(t);
      int tempBaseBase = da.getBase(tempBase);
      da.setBase(tempBase, q);
      int ss = s;
      for (int j = 0, n = list1.size(); j < n; j++) {
        int c = list1.get(j);
        if (tempBaseBase + c == s) {
          ss = q + c;
        }
        if (da.getCheck(tempBaseBase + c) == tempBase) {
          copyEntry(c, tempBaseBase + c, q + c);
        }
      }
      da.setCheck(t, ss);
      da.setTail(t, word.substring(i + 1));
    } else {
      List<Integer> list = new ArrayList<>();
      list.add(code);
      list.addAll(list3);
      int q = da.xCheck(list);
      int tempBaseBase = da.getBase(s);
      da.setBase(s, q);
      for (int j = 0, n = list3.size(); j < n; j++) {
        int c = list3.get(j);
        if (da.getCheck(tempBaseBase + c) == s) {
          copyEntry(n, tempBaseBase + c, q + c);
        }
      }
      da.setCheck(q + code, s);
      da.setTail(q + code, word.substring(i + 1));
    }
  }

  private void copyEntry(int n, int k, int l) {
    int currentBase = da.getBase(k);
    da.copy(k, l);
    for (int m = 1; m <= alphabet.size(); m++) {
      if (da.getCheck(m + currentBase) == k) {
        da.setCheck(m + currentBase, l);
      }
    }
  }

  private void collisionInTail(int t, String tail) {
    String tail1 = da.getTail(t);
    int tt = t;
    int j = 0;
    for (int n = Math.min(tail.length(), tail1.length()); j < n; j++) {
      if (tail.charAt(j) != tail1.charAt(j)) {
        break;
      }
      int codeJ = alphabet.code(tail.charAt(j));
      int q = da.xCheck(codeJ);
      da.setTail(t, null);
      da.setBase(t, q);
      tt = q + codeJ;
      da.setCheck(tt, t);
      t = tt;
    }
    int codeJ = alphabet.code(tail.charAt(j));
    int codeJ1 = alphabet.code(tail1.charAt(j));

    int q = da.xCheck(codeJ, codeJ1);
    da.setBase(tt, q);
    da.setTail(tt, null);

    da.setCheck(q + codeJ, tt);
    da.setTail(q + codeJ, tail.substring(j + 1));

    da.setCheck(q + codeJ1, tt);
    da.setTail(q + codeJ1, tail1.substring(j + 1));
  }


  @Override
  public boolean contains(Object value) {
      return value instanceof String string && containsWord(string);
  }

  private boolean containsWord(String value) {
    String word = alphabet.checkEot(value);
    int s = 1;
    for (int i = 0; i < word.length(); i++) {
      int code = alphabet.code(word.charAt(i));
      int t = da.getBase(s) + code;
      if (da.getCheck(t) != s) {
        return false;
      }
      final String tail = da.getTail(t);
      if (tail != null) {
        return tail.equals(word.substring(i + 1));
      }
      s = t;
    }
    return false;
  }

  @Override
  public boolean containsPrefix(String value) {
    String prefix = alphabet.check(value);
    int s = 1;
    for (int i = 0; i < prefix.length(); i++) {
      int code = alphabet.code(prefix.charAt(i));
      int t = da.getBase(s) + code;
      if (da.getCheck(t) != s) {
        return false;
      }
      final String tail = da.getTail(t);
      if (tail != null) {
        return tail.startsWith(prefix.substring(i + 1));
      }
      s = t;
    }
    return true;
  }

  @Override
  public boolean prefixed(String word) {
    String prefix = alphabet.check(word);
    int s = 1;
    for (int i = 0; i < prefix.length(); i++) {
      int code = alphabet.code(prefix.charAt(i));
      int t = da.getBase(s) + code;
      if (da.getCheck(t) != s) {
        return false;
      }
      final String tail = da.getTail(t);
      if (tail != null) {
        return prefix.startsWith(tail.substring(0, tail.length() - 1), i + 1);
      }
      s = t;
    }
    return true;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return c.stream().allMatch(this::contains);
  }

  @Override
  public boolean addAll(Collection<? extends String> c) {
    return c.stream().map(this::add).reduce(Boolean::logicalAnd).orElse(false);
  }

  @Override
  public Iterator<String> iterator() {
    return new DoubleArrayIterator<>(this);
  }

  @Override
  public Iterator<String> iterator(String value) {
    String prefix = alphabet.check(value);
    int s = 1;
    for (int i = 0; i < prefix.length(); i++) {
      int code = alphabet.code(prefix.charAt(i));
      int t = da.getBase(s) + code;
      if (da.getCheck(t) != s) {
        return Collections.emptyIterator();
      }
      if (da.getTail(t) != null) {
        LOGGER.info("prefix.substring(0, i): {}, {}, {}", t, i, prefix.substring(0, i));
        return new DoubleArrayIterator<>(this, s, t - 1, prefix.substring(0, i));
      }
      s = t;
    }

    return new DoubleArrayIterator<>(this, s, 1, prefix);
  }

  @Override
  public void clear() {
    da.clear();
    da.setBase(1, 1);
    size = 0;
  }

  @Override
  public boolean remove(Object value) {
    if (!(value instanceof String)) {
      return false;
    }
    LOGGER.debug("remove={}", value);
    String word = alphabet.checkEot((String) value);
    int s = 1;
    for (int i = 0; i < word.length(); i++) {
      int code = alphabet.code(word.charAt(i));
      int baseS = da.getBase(s);
      int t = baseS + code;
      int checkT = da.getCheck(t);
      if (checkT != s) {
        return false;
      }
      String tail1 = da.getTail(t);
      if (tail1 != null) {
        if (word.substring(i + 1).equals(tail1)) {
          da.setTail(t, null);
          size--;
          return remove(t);
        }
        return false;
      }
      s = t;
    }
    return remove(s);
  }

  private boolean remove(int s) {
    int check;
    do {
      check = da.getCheck(s);
      da.setCheck(s, 0);
      s = check;
    } while (check != 1 && da.getArcs(s, alphabet.size()).isEmpty());
    return true;
  }

  public String toString() {
    return super.toString() + " size=" + size;
  }
}
