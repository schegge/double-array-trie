package de.schegge.collection.dat;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Double array implementation of the {@link Map} interface. The type of the keys in this implementation is {@link String}.
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access an <tt>DoubleArrayTrie</tt> instance concurrently, and at least one of the threads
 * modifies the trie structurally, it <i>must</i> be synchronized externally.
 *
 * @param <V> the type of mapped values
 */
public class DoubleArrayMap<V> implements Map<String, V> {

    DoubleArray<EntryNode<V>> da;
    Alphabet alphabet;
    int size;

    public DoubleArrayMap() {
        this(Alphabet.with().letters().lowercase().build());
    }

    public DoubleArrayMap(String characters) {
        this(Alphabet.with().letters(characters).build());
    }

    public DoubleArrayMap(Alphabet alphabet) {
        da = new DoubleArray<>(EntryNode::new);
        da.setBase(1, 1);
        this.alphabet = alphabet;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private int collisionInArray(String word, int s, int i, int code, int t) {
        List<Integer> list3 = da.getArcs(s, alphabet.size());
        List<Integer> list1 = da.getArcs(da.getCheck(t), alphabet.size());
        if (list3.size() + 1 >= list1.size()) {
            int q = da.xCheck(list1);
            int tempBase = da.getCheck(t);
            int tempBaseBase = da.getBase(tempBase);
            da.setBase(tempBase, q);
            int ss = s;
            for (int j = 0, n = list1.size(); j < n; j++) {
                int c = list1.get(j);
                if (tempBaseBase + c == s) {
                    ss = q + c;
                }
                if (da.getCheck(tempBaseBase + c) == tempBase) {
                    copyEntry(tempBaseBase + c, q + c);
                }
            }
            da.setCheck(t, ss);
            da.setTail(t, word.substring(i + 1));
            return t;
        } else {
            List<Integer> list = new ArrayList<>();
            list.add(code);
            list.addAll(list3);
            int q = da.xCheck(list);
            int tempBaseBase = da.getBase(s);
            da.setBase(s, q);
            for (int j = 0, n = list3.size(); j < n; j++) {
                int c = list3.get(j);
                if (da.getCheck(tempBaseBase + c) == s) {
                    copyEntry(tempBaseBase + c, q + c);
                }
            }
            da.setCheck(q + code, s);
            da.setTail(q + code, word.substring(i + 1));
            return q + code;
        }
    }

    private void copyEntry(int k, int l) {
        int currentBase = da.getBase(k);
        da.copy(k, l);
        da.get(l).setValue(da.get(k).getValue());
        da.get(k).setValue(null);
        for (int m = 1; m <= alphabet.size(); m++) {
            if (da.getCheck(m + currentBase) == k) {
                da.setCheck(m + currentBase, l);
            }
        }
    }

    private int collisionInTail(int t, String tail) {
        String tail1 = da.getTail(t);
        V value = da.get(t).getValue();
        int tt = t;
        int j = 0;
        for (int n = Math.min(tail.length(), tail1.length()); j < n; j++) {
            if (tail.charAt(j) != tail1.charAt(j)) {
                break;
            }
            int codeJ = alphabet.code(tail.charAt(j));
            int q = da.xCheck(codeJ);
            da.setTail(t, null);
            da.get(t).setValue(null);
            da.setBase(t, q);
            tt = q + codeJ;
            da.setCheck(tt, t);
            t = tt;
        }
        int codeJ = alphabet.code(tail.charAt(j));
        int codeJ1 = alphabet.code(tail1.charAt(j));

        int q = da.xCheck(codeJ, codeJ1);
        da.setBase(tt, q);
        da.setTail(tt, null);

        da.setCheck(q + codeJ, tt);
        da.setTail(q + codeJ, tail.substring(j + 1));

        da.setCheck(q + codeJ1, tt);
        da.setTail(q + codeJ1, tail1.substring(j + 1));
        da.get(q + codeJ1).setValue(value);
        return q + codeJ;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        da = new DoubleArray<>(EntryNode::new);
        da.setBase(1, 1);
        size = 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return containsAt((String) key) != 0;
    }

    @Override
    public boolean containsValue(Object value) {
        for (int i = 1; i <= da.size(); i++) {
            if (Objects.equals(da.get(i).getValue(), value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        if (!(key instanceof String)) {
            return null;
        }
        int index = containsAt((String) key);
        return index == 0 ? null : da.get(index).getValue();
    }

    @Override
    public V put(String key, V value) {
        int index = add(key);
        V result = da.get(index).getValue();
        da.get(index).setValue(value);
        return result;
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends String, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public Set<String> keySet() {
        return Tries.unmodifiable(new KeySet<>(da, alphabet, size));
    }

    @Override
    public Collection<V> values() {
        return Collections.unmodifiableCollection(new FastValues());
    }

    @Override
    public Set<Entry<String, V>> entrySet() {
        return Collections.unmodifiableSet(new EntrySet());
    }

    private int add(String value) {
        String word = alphabet.checkEot(value);
        int s = 1;
        for (int i = 0; i < word.length(); i++) {
            int code = alphabet.code(word.charAt(i));
            int baseS = da.getBase(s);
            int t = baseS + code;
            int checkT = da.getCheck(t);
            if (checkT == 0) {
                String tail = word.substring(i + 1);
                da.setCheck(t, s);
                da.setTail(t, tail);
                size++;
                return t;
            }
            if (checkT != s) {
                int result = collisionInArray(word, s, i, code, t);
                size++;
                return result;
            }
            String tail1 = da.getTail(t);
            if (tail1 != null) {
                String tail = word.substring(i + 1);
                if (tail.equals(tail1)) {
                    return t;
                }
                int result = collisionInTail(t, tail);
                size++;
                return result;
            }
            s = t;
        }
        return s;
    }

    int containsAt(String value) {
        String word = alphabet.checkEot(value);
        int s = 1;
        for (int i = 0; i < word.length(); i++) {
            int code = alphabet.code(word.charAt(i));
            int t = da.getBase(s) + code;
            if (da.getCheck(t) != s) {
                return 0;
            }
            final String tail = da.getTail(t);
            if (tail != null) {
                return tail.equals(word.substring(i + 1)) ? t : 0;
            }
            s = t;
        }
        return 0;
    }

    static class KeySet<V> extends AbstractDoubleArrayTrie<EntryNode<V>> {

        KeySet(DoubleArray<EntryNode<V>> da, Alphabet alphabet, int size) {
            super(da, alphabet, size);
        }
    }

    class EntrySet extends AbstractSet<Map.Entry<String, V>> {

        @Override
        public Iterator<Entry<String, V>> iterator() {
            return new EntryIterator();
        }

        @Override
        public int size() {
            return size;
        }
    }

    class EntryIterator implements Iterator<Map.Entry<String, V>> {

        DoubleArrayIterator<EntryNode<V>> wrapped = new DoubleArrayIterator<>(new KeySet<>(da, alphabet, size));

        @Override
        public boolean hasNext() {
            return wrapped.hasNext();
        }

        @Override
        public Entry<String, V> next() {
            String key = wrapped.next();
            if (key == null) {
                return null;
            }
            return new Entry<>() {

                @Override
                public String getKey() {
                    return key;
                }

                @Override
                public V getValue() {
                    int index = containsAt(key);
                    return index == 0 ? null : da.get(index).getValue();
                }

                @Override
                public V setValue(V value) {
                    throw new UnsupportedOperationException();
                }
            };
        }
    }

    class FastValues extends AbstractCollection<V> {

        @Override
        public Iterator<V> iterator() {
            return new FastValueIterator();
        }

        @Override
        public int size() {
            return size;
        }
    }

    class FastValueIterator implements Iterator<V> {

        int currentIndex = first();

        private int first() {
            for (int i = 1; i <= da.size(); i++) {
                if (da.get(i).getValue() != null) {
                    return i;
                }
            }
            return da.size() + 1;
        }

        @Override
        public boolean hasNext() {
            return currentIndex <= da.size();
        }

        @Override
        public V next() {
            EntryNode<V> node = da.get(currentIndex);
            V value = node.getValue();
            currentIndex++;
            while (currentIndex <= da.size()) {
                if (da.get(currentIndex).getValue() != null) {
                    break;
                }
                currentIndex++;
            }
            return value;
        }
    }
}
