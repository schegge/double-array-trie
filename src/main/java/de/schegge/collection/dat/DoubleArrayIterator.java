package de.schegge.collection.dat;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class DoubleArrayIterator<N extends DoubleNode> implements Iterator<String> {

  private final AbstractDoubleArrayTrie<N> dat;

  private String next;
  private String previous;
  private final Deque<State> stack = new LinkedList<>();

  DoubleArrayIterator(AbstractDoubleArrayTrie<N> dat) {
    this(dat, 1, 1, "");
  }

  DoubleArrayIterator(AbstractDoubleArrayTrie<N> dat, int base, int code, String prefix) {
    this.dat = dat;
    stack.push(new State(base, code, prefix));
    next = findNext();
  }

  @Override
  public boolean hasNext() {
    return next != null;
  }

  @Override
  public String next() {
    previous = next;
    next = findNext();
    return previous;
  }

  private String findNext() {
    while (!stack.isEmpty()) {
      State state = stack.peek();
      int base = dat.da.getBase(state.getBaseIndex());
      boolean push = false;
      for (int i = state.getCode(); i <= dat.alphabet.size(); i++) {
        if (dat.da.getCheck(base + i) == state.getBaseIndex()) {
          String tail = dat.da.getTail(base + i);
          stack.poll();
          stack.push(new State(state.baseIndex, i + 1, state.prefix));
          if (tail != null) {
            return removeEot(state.prefix + dat.alphabet.alpha(i) + tail);
          }
          stack.push(new State(base + i, 1, state.getPrefix() + dat.alphabet.alpha(i)));
          push = true;
          break;
        }
      }
      if (!push) {
        stack.poll();
      }
    }
    return null;
  }

  private String removeEot(String s) {
    return s != null ? s.substring(0, s.length() - 1) : null;
  }

  @Override
  public void remove() {
    dat.remove(previous);
  }

  private static class State {

    private final int baseIndex;
    private final int code;
    private final String prefix;

    State(int baseIndex, int code, String prefix) {
      this.baseIndex = baseIndex;
      this.code = code;
      this.prefix = prefix;
    }

    int getBaseIndex() {
      return baseIndex;
    }

    int getCode() {
      return code;
    }

    String getPrefix() {
      return prefix;
    }
  }
}
