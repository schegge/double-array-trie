/*
 * Tries.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.collection.dat;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Utility methods for {@link Trie}.
 */
public final class Tries {

  private Tries() {
    super();
  }

  static class UnmodifiableTrie implements Trie {

    private final Trie wrapped;

    UnmodifiableTrie(Trie wrapped) {
      this.wrapped = wrapped;
    }

    @Override
    public Iterator<String> iterator(String prefix) {
      return new UnmodifiableIterator(wrapped.iterator(prefix));
    }

    @Override
    public Iterator<String> iterator() {
      return new UnmodifiableIterator(wrapped.iterator());
    }

    @Override
    public boolean containsPrefix(String prefix) {
      return wrapped.containsPrefix(prefix);
    }

    @Override
    public boolean prefixed(String word) {
      return wrapped.prefixed(word);
    }

    @Override
    public int size() {
      return wrapped.size();
    }

    @Override
    public boolean isEmpty() {
      return wrapped.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
      return wrapped.contains(o);
    }

    @Override
    public Object[] toArray() {
      return wrapped.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
      return wrapped.toArray(a);
    }

    @Override
    public boolean add(String s) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
      return wrapped.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
      throw new UnsupportedOperationException();

    }

    @Override
    public boolean retainAll(Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
      throw new UnsupportedOperationException();
    }

    private static class UnmodifiableIterator implements Iterator<String> {

      private final Iterator<String> wrapped;

      public UnmodifiableIterator(Iterator<String> wrapped) {
        this.wrapped = wrapped;
      }

      public boolean hasNext() {
        return wrapped.hasNext();
      }

      public String next() {
        return wrapped.next();
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }

      @Override
      public void forEachRemaining(Consumer<? super String> action) {
        wrapped.forEachRemaining(action);
      }
    }
  }

  /**
   * Returns an unmodifiable view of the specified trie. Query operations on the returned trie "read through" to the
   * specified map, and attempts to modify the returned trie, whether direct or via its collection views, result in an
   * <tt>UnsupportedOperationException</tt>.<p>
   *
   * @param trie the trie for which an unmodifiable view is to be returned.
   * @return an unmodifiable view of the specified trie.
   */
  public static Trie unmodifiable(Trie trie) {
    return new UnmodifiableTrie(trie);
  }

  /**
   * Returns a synchronized (thread-safe) trie backed by the specified trie. In order to guarantee serial access, it is
   * critical that
   * <strong>all</strong> access to the backing trie is accomplished
   * through the returned trie.<p>
   *
   * @param t the trie to be "wrapped" in a synchronized trie.
   * @return a synchronized view of the specified trie.
   */
  public static Trie synchronizedTrie(Trie t) {
    return new SynchronizedTrie(t);
  }

  static class SynchronizedTrie implements Trie {

    final Trie c;
    final Object mutex;

    SynchronizedTrie(Trie c) {
      this.c = Objects.requireNonNull(c);
      mutex = this;
    }

    SynchronizedTrie(Trie c, Object mutex) {
      this.c = Objects.requireNonNull(c);
      this.mutex = Objects.requireNonNull(mutex);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      synchronized (mutex) {
        return c.equals(o);
      }
    }

    public int hashCode() {
      synchronized (mutex) {
        return c.hashCode();
      }
    }

    public int size() {
      synchronized (mutex) {
        return c.size();
      }
    }

    public boolean isEmpty() {
      synchronized (mutex) {
        return c.isEmpty();
      }
    }

    public boolean contains(Object o) {
      synchronized (mutex) {
        return c.contains(o);
      }
    }

    public Object[] toArray() {
      synchronized (mutex) {
        return c.toArray();
      }
    }

    public <T> T[] toArray(T[] a) {
      synchronized (mutex) {
        return c.toArray(a);
      }
    }

    public Iterator<String> iterator() {
      return c.iterator();
    }

    public boolean add(String e) {
      synchronized (mutex) {
        return c.add(e);
      }
    }

    public boolean remove(Object o) {
      synchronized (mutex) {
        return c.remove(o);
      }
    }

    public boolean containsAll(Collection<?> coll) {
      synchronized (mutex) {
        return c.containsAll(coll);
      }
    }

    public boolean addAll(Collection<? extends String> coll) {
      synchronized (mutex) {
        return c.addAll(coll);
      }
    }

    public boolean removeAll(Collection<?> coll) {
      synchronized (mutex) {
        return c.removeAll(coll);
      }
    }

    public boolean retainAll(Collection<?> coll) {
      synchronized (mutex) {
        return c.retainAll(coll);
      }
    }

    public void clear() {
      synchronized (mutex) {
        c.clear();
      }
    }

    public String toString() {
      synchronized (mutex) {
        return c.toString();
      }
    }

    @Override
    public void forEach(Consumer<? super String> consumer) {
      synchronized (mutex) {
        c.forEach(consumer);
      }
    }

    @Override
    public boolean removeIf(Predicate<? super String> filter) {
      synchronized (mutex) {
        return c.removeIf(filter);
      }
    }

    @Override
    public Spliterator<String> spliterator() {
      return c.spliterator();
    }

    @Override
    public Iterator<String> iterator(String prefix) {
      synchronized (mutex) {
        return c.iterator(prefix);
      }
    }

    @Override
    public boolean containsPrefix(String prefix) {
      synchronized (mutex) {
        return c.containsPrefix(prefix);
      }
    }

    @Override
    public boolean prefixed(String word) {
      synchronized (mutex) {
        return c.prefixed(word);
      }
    }

    @Override
    public Stream<String> stream() {
      return c.stream();
    }

    @Override
    public Stream<String> parallelStream() {
      return c.parallelStream();
    }
  }
}
