package de.schegge.collection.dat;

interface DoubleNode {
	int getBase();

	void setBase(int base);

	int getCheck();

	void setCheck(int check);

	String getTail();

	void setTail(String tail);
}
