package de.schegge.collection.dat;

import java.text.Collator;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

public class Alphabet {
  private final String alphabet;
  private final char eot;

  public static class Builder {

    String alphabet;

    private Builder(String alphabet) {
      this.alphabet = alphabet;
    }

    public LetterBuilder letters() {
      return new LetterBuilder(toString(), "abcdefghijklmnopqrstuvwxyz");
    }

    public FixLetterBuilder letters(String letters) {
      return new FixLetterBuilder(toString(), letters);
    }

    public Builder digits() {
      return new Builder(this + "0123456789");
    }

    public Alphabet build() {
      return new Alphabet(toString());
    }

    public Alphabet build(Locale locale) {
      return new Alphabet(sortCharacters(toString(), Collator.getInstance(locale)));
    }

    private String sortCharacters(String alphabet, Collator collator) {
      return Arrays.stream(alphabet.split("")).sorted(collator).collect(Collectors.joining());
    }

    public String toString() {
      return alphabet;
    }
  }

  public static class LetterBuilder extends Builder {

    private final String lowerCase;
    private final String upperCase;

    private LetterBuilder(String alphabet, String lowerCase) {
      this(alphabet, lowerCase, lowerCase.toUpperCase());
    }

    private LetterBuilder(String alphabet, String lowerCase, String upperCase) {
      super(alphabet);
      this.lowerCase = lowerCase;
      this.upperCase = upperCase;
    }

    public LetterBuilder uppercase() {
      return new LetterBuilder(alphabet, "", upperCase);
    }

    public LetterBuilder lowercase() {
      return new LetterBuilder(alphabet, lowerCase, "");
    }

    @java.lang.Override
    public String toString() {
      return alphabet + lowerCase + upperCase;
    }
  }

  public static class FixLetterBuilder extends Builder {

    private final String letters;

    private FixLetterBuilder(String alphabet, String letters) {
      super(alphabet);
      this.letters = letters;
    }

    public LetterBuilder uppercase() {
      return new LetterBuilder(alphabet, "", letters.toUpperCase());
    }

    public LetterBuilder lowercase() {
      return new LetterBuilder(alphabet, letters.toLowerCase(), "");
    }

    @java.lang.Override
    public String toString() {
      return alphabet + letters;
    }
  }

  private Alphabet(String alphabet) {
    eot = '\u0004';
    if (alphabet.indexOf(eot) != -1) {
      throw new IllegalArgumentException("EOT character contained in alphabet");
    }
    this.alphabet = eot + alphabet;
  }

  public static Builder with() {
    return new Builder("");
  }

  int code(char c) {
    return alphabet.indexOf(c) + 1;
  }

  char alpha(int code) {
    return alphabet.charAt(code - 1);
  }

  public int size() {
    return alphabet.length();
  }

  String checkEot(String word) {
    return check(word) + eot;
  }

  String check(String word) {
    if (!isValid(word)) {
      throw new IllegalArgumentException("invalid word: " + word);
    }
    return word;
  }

  private boolean isValid(String word) {
    for (char aChar : word.toCharArray()) {
      if (alphabet.indexOf(aChar) < 1) {
        return false;
      }
    }
    return true;
  }

  public String getAlphabet() {
    return alphabet.substring(1);
  }
}
