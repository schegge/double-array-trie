package de.schegge.collection.dat;

class BaseNode implements DoubleNode {
	private int base;
	private int check;
	private String tail;

	@Override
	public int getBase() {
		return base;
	}

	@Override
	public int getCheck() {
		return check;
	}

	@Override
	public String getTail() {
		return tail;
	}

	@Override
	public void setBase(int base) {
		this.base = base;
	}

	@Override
	public void setCheck(int check) {
		this.check = check;
	}

	@Override
	public void setTail(String tail) {
		this.tail = tail;
	}
}
