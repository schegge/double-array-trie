package de.schegge.collection.dat;

class EntryNode<V> extends BaseNode {
	private V value;

	V getValue() {
		return value;
	}

	void setValue(V value) {
		this.value = value;
	}
}
