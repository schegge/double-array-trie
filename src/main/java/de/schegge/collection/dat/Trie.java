package de.schegge.collection.dat;

import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Trie extends Set<String> {
    @Override
    default Spliterator<String> spliterator() {
        return Spliterators.spliterator(this, Spliterator.ORDERED | Spliterator.SORTED | Spliterator.DISTINCT);
    }

    /**
     * Returns a spliterator over the elements contained in this trie, having a key with the specified prefix.
     *
     * @param prefix the kex prefix
     * @return a spliterator over the elements contained in this trie, having a key with the specified prefix
     */
    default Spliterator<String> spliterator(String prefix) {
        return Spliterators.spliterator(iterator(prefix), size(),
                Spliterator.ORDERED | Spliterator.SORTED | Spliterator.DISTINCT);
    }

    /**
     * Returns a stream over the elements contained in this trie, having a key with the specified prefix.
     *
     * @param prefix the key prefix
     * @return a stream over the elements contained in this trie, having a key with the specified prefix
     */
    default Stream<String> stream(String prefix) {
        return StreamSupport.stream(spliterator(prefix), false);
    }

    /**
     * Returns an iterator over the elements contained in this trie, having a key with the specified prefix.
     *
     * @param prefix the key prefix
     * @return an iterator over the elements contained in this trie, having a key with the specified prefix
     */
    Iterator<String> iterator(String prefix);

    /**
     * Returns <tt>true</tt> if this trie contains a key with the specified prefix.
     *
     * @param prefix the key prefix whose presence in this trie is to be tested
     * @return <tt>true</tt> if this trie contains the specified key prefix
     * @throws NullPointerException if the specified prefix is null
     */
    boolean containsPrefix(String prefix);

    /**
     * Returns <tt>true</tt> if this trie contains a key which is the start of the specified word.
     *
     * @param word the word whose prefix presence in this trie is to be tested
     * @return <tt>true</tt> if this trie contains the start of the specified word
     * @throws NullPointerException if the specified word is null
     */
    boolean prefixed(String word);
}
