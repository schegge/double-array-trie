package de.schegge.collection.dat;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class DoubleArray<N extends DoubleNode> {

  private final List<N> entries;
  private final List<Integer> empty;
  private final Supplier<N> supplier;

  DoubleArray(Supplier<N> supplier) {
    entries = new ArrayList<>();
    empty = new ArrayList<>();
    this.supplier = supplier;
  }

  N get(int index) {
    return entries.get(index - 1);
  }

  void setBase(int index, int base) {
    check(index);
    entries.get(index - 1).setBase(base);
  }

  int getBase(int index) {
      return index <= entries.size() ? entries.get(index - 1).getBase() : 0;
  }

  void setCheck(int index, int check) {
    check(index);
    final N n = entries.get(index - 1);
    int oldCheck = n.getCheck();
    n.setCheck(check);
    if (check != 0 && oldCheck == 0) {
      empty.remove((Integer) index);
    }
  }

  int getCheck(int index) {
    if (index <= entries.size()) {
      return entries.get(index - 1).getCheck();
    }
    return 0;
  }


  void setTail(int index, String tail) {
    entries.get(index - 1).setTail(tail);
  }

  String getTail(int index) {
      return index <= entries.size() ? entries.get(index - 1).getTail() : null;
  }

  private void check(int index) {
    while (index > entries.size()) {
      entries.add(supplier.get());
      empty.add(entries.size());
    }
  }

  int size() {
    return entries.size();
  }

  List<Integer> getArcs(int t, int size) {
    int base = entries.get(t - 1).getBase();
    List<Integer> result = new ArrayList<>();
    for (int i = base, l = Math.min(base + size, entries.size()); i <= l; i++) {
      if (entries.get(i - 1).getCheck() == t) {
        result.add(i - base);
      }
    }
    return result;
  }

  void copy(int sourceIndex, int targetIndex) {
    check(targetIndex);
    DoubleNode source = get(sourceIndex);
    DoubleNode target = get(targetIndex);
    target.setBase(source.getBase());
    target.setCheck(source.getCheck());
    target.setTail(source.getTail());
    empty.add(sourceIndex);
    empty.remove((Integer) targetIndex);
    source.setBase(0);
    source.setCheck(0);
    source.setTail(null);
  }

  int xCheck(int code) {
    for (int e : empty) {
      if (e > code && getCheck(e) == 0) {
        return e - code;
      }
    }
    return size(); //empty.stream().map(e -> e - code).filter(q -> q > 0).findFirst().orElseGet(this::size);
  }

  int xCheck(int code1, int code2) {
    int code = Math.min(code1, code2);
    for (int e : empty) {
      int q = e - code;
      if (q > 0 && getCheck(q + code1) == 0 && getCheck(q + code2) == 0) {
        return q;
      }
    }
    return size();
  }

  int xCheck(List<Integer> codes) {
    int code = codes.get(0);
    for (int e : empty) {
      int q = e - code;
      if (q > 0 && xCheck(codes, q)) {
        return q;
      }
    }
    return size();
  }

  private boolean xCheck(List<Integer> codes, int q) {
    for (int code : codes) {
      if (getCheck(q + code) != 0) {
        return false;
      }
    }
    return true;
  }

  void clear() {
    entries.clear();
    empty.clear();
  }
}
