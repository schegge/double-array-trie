/*
 * OrderedDoubleArrayMap.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.collection.dat;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * Double array implementation of the {@link Map} interface. The type of the keys in this implementation is {@link String}.
 * The order in the values collection is the order of the keys in the trie.
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access an <tt>DoubleArrayTrie</tt> instance concurrently, and at least one of the threads
 * modifies the trie structurally, it <i>must</i> be synchronized externally.
 *
 *  @param <V> the type of mapped values
 */
public class OrderedDoubleArrayMap<V> extends DoubleArrayMap<V> {

  @Override
  public Collection<V> values() {
    return Collections.unmodifiableCollection(new Values());
  }

  class Values extends AbstractCollection<V> {

    @Override
    public Iterator<V> iterator() {
      return new ValueIterator();
    }

    @Override
    public int size() {
      return size;
    }
  }

  class ValueIterator implements Iterator<V> {

    DoubleArrayIterator<EntryNode<V>> wrapped = new DoubleArrayIterator<>(new KeySet<>(da, alphabet, size));

    @Override
    public boolean hasNext() {
      return wrapped.hasNext();
    }

    @Override
    public V next() {
      String key = wrapped.next();
      if (key == null) {
        return null;
      }
      int index = containsAt(key);
      return index == 0 ? null : da.get(index).getValue();
    }
  }
}
