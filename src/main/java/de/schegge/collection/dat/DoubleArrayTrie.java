package de.schegge.collection.dat;

/**
 * Double array implementation of the {@link Trie} interface.
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access an <tt>DoubleArrayTrie</tt> instance concurrently, and at least one of the threads
 * modifies the trie structurally, it <i>must</i> be synchronized externally.
 */
public class DoubleArrayTrie extends AbstractDoubleArrayTrie<BaseNode> {
  
  public DoubleArrayTrie() {
    this(Alphabet.with().letters().lowercase().build());
  }

  public DoubleArrayTrie(String characters) {
    this(Alphabet.with().letters(characters).build());
  }

  public DoubleArrayTrie(Alphabet alphabet) {
    super(alphabet, BaseNode::new);
  }
}
