package de.schegge.collection;

import static java.util.Locale.GERMAN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import de.schegge.collection.dat.Alphabet;
import de.schegge.collection.dat.Alphabet.Builder;
import de.schegge.collection.dat.Alphabet.LetterBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AlphabetBuilderTest {

  private LetterBuilder builder;

  @BeforeEach
  void setupBuilder() {
    builder = Alphabet.with().letters();
  }

  @Test
  void buildWithLetters() {
    assertEquals("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", builder.build().getAlphabet());
  }

  @Test
  void buildWithLowerCaseLetters() {
    assertEquals("abcdefghijklmnopqrstuvwxyz", builder.lowercase().build().getAlphabet());
  }

  @Test
  void buildWithUpperCaseLetters() {
    assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZ", builder.uppercase().build().getAlphabet());
  }

  @Test
  void buildWithLettersAndDigits() {
    assertEquals("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        Alphabet.with().letters().digits().build().getAlphabet());
  }

  @Test
  void buildWithLettersAndDigitsAndGermanLocale() {
    assertEquals("0123456789aAäÄbBcCdDeEfFgGhHiIjJkKlLmMnNoOöÖpPqQrRsSßtTuUüÜvVwWxXyYzZ",
        builder.letters("äöüÄÖÜß").digits().build(GERMAN).getAlphabet());
  }

  @Test
  void buildWithLowerCaseUmlaute() {
    assertEquals("äöüß", Alphabet.with().letters("ÄöÜß").lowercase().build().getAlphabet());
  }

  @Test
  void buildWithLowerCaseLettersAndDigits() {
    assertEquals("abcdefghijklmnopqrstuvwxyz0123456789", builder.lowercase().digits().build().getAlphabet());
  }

  @Test
  void buildWithUpperCaseLettersAndDigits() {
    assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", builder.uppercase().digits().build().getAlphabet());
  }

  @Test
  void buildWithUpperCaseLettersAndDigitsAndUppercaseUmlaute() {
    assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ0123456789",
        builder.uppercase().letters("äöü").uppercase().digits().build().getAlphabet());
  }
}
