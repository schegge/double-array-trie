package de.schegge.collection.dat;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OrderedDoubleArrayMapTest {

  private Map<String, Integer> map;

  @BeforeEach
  void setUp() {
    map = new OrderedDoubleArrayMap<>();
  }

  @Test
  void valueSet() {
    map.put("bachelor", 1);
    map.put("jar", 2);
    map.put("badge", 3);
    map.put("baby", 4);
    assertEquals(asList(4, 1, 3, 2), new ArrayList<>(map.values()));
  }

}