package de.schegge.collection.dat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingInt;
import static java.util.Locale.GERMANY;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DoubleArrayMapTest {

    private Map<String, Integer> map;

    @BeforeEach
    void setUp() {
        map = new DoubleArrayMap<>();
    }

    @Test
    void emptyMap() {
        assertTrue(map.isEmpty());
        assertEquals(0, map.size());
        assertNull(map.get("bachelor"));
    }

    @Test
    void clear() {
        map.put("bachelor", 1);
        assertFalse(map.isEmpty());
        assertEquals(1, map.size());
        assertEquals(1, map.get("bachelor"));
        map.clear();
        assertTrue(map.isEmpty());
        assertEquals(0, map.size());
        assertNull(map.get("bachelor"));
    }

    @Test
    void nullKey() {
        assertNull(map.get(null));
    }

    @Test
    void oneWordMap() {
        map.put("bachelor", 1);
        assertFalse(map.isEmpty());
        assertEquals(1, map.size());
        assertEquals(1, map.get("bachelor"));
    }

    @Test
    void fourWordMap() {
        map.put("bachelor", 1);
        map.put("jar", 2);
        map.put("badge", 3);
        map.put("baby", 4);
        assertFalse(map.isEmpty());
        assertEquals(4, map.size());
        assertEquals(1, map.get("bachelor"));
        assertEquals(2, map.get("jar"));
        assertEquals(3, map.get("badge"));
        assertEquals(4, map.get("baby"));
        assertTrue(map.containsKey("bachelor"));
        assertTrue(map.containsKey("jar"));
        assertTrue(map.containsKey("badge"));
        assertTrue(map.containsKey("baby"));
        assertFalse(map.containsKey("bank"));
    }

    @Test
    void countText() {
        final Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜ-").build(GERMANY);
        Map<String, AtomicInteger> map = new DoubleArrayMap<>(alphabet);
        try (Scanner s = new Scanner(getClass().getResourceAsStream("/dieverwandlung.txt"))) {
            while (s.hasNext()) {
                final String word = s.next().replaceAll("[.,;:!?»«]", "");
                if (word.matches("^[a-zA-ZaäöüßÄÖÜ-]+$")) {
                    map.computeIfAbsent(word, k -> new AtomicInteger()).getAndIncrement();
                }
            }
        }
        String result = map.entrySet().stream().sorted(reverseOrder(comparingInt(a -> a.getValue().get())))
                .map(e -> e.getKey() + "=" + e.getValue()).collect(joining(", "));
        assertEquals(56190, result.length());
    }

    @Test
    void entrySet() {
        map.put("bachelor", 1);
        map.put("jar", 2);
        map.put("badge", 3);
        map.put("baby", 4);
        assertEquals(asList("baby=4", "bachelor=1", "badge=3", "jar=2"),
                map.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(toList()));
    }

    @Test
    void keySet() {
        map.put("bachelor", 1);
        map.put("jar", 2);
        map.put("badge", 3);
        map.put("baby", 4);
        assertEquals(asList("baby", "bachelor", "badge", "jar"), new ArrayList<>(map.keySet()));
    }

    @Test
    void valueSet() {
        map.put("bachelor", 1);
        map.put("jar", 2);
        map.put("badge", 3);
        map.put("baby", 4);
        assertEquals(asList(4, 1, 3, 2), new ArrayList<>(map.values()));
    }

    @Test
    void putAll() {
        map.putAll(Stream.of("bachelor", "jar", "badge", "baby").collect(toMap(identity(), String::length)));
        assertEquals(asList(4, 8, 5, 3), new ArrayList<>(map.values()));
    }

    @Test
    void containsKey() {
        map.putAll(Stream.of("bachelor", "jar", "badge", "baby").collect(toMap(identity(), String::length)));
        assertTrue(map.containsKey("baby"));
        assertFalse(map.containsKey("bank"));
    }

    @Test
    void containsValue() {
        map.putAll(Stream.of("bachelor", "jar", "badge", "baby").collect(toMap(identity(), String::length)));
        assertTrue(map.containsValue(3));
        assertFalse(map.containsValue(42));
        assertFalse(map.containsValue("test"));
    }

    @Test
    void unsupportedOperation() {
        assertThrows(UnsupportedOperationException.class, () -> map.remove("test"));
        assertThrows(UnsupportedOperationException.class, () -> map.remove("bachelor", 1));
    }
}
