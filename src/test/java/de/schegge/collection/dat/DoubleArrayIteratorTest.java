package de.schegge.collection.dat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

class DoubleArrayIteratorTest {
	@Test
	void emptyTrie() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		Iterator<String> iterator = doubleArrayTrie.iterator();
		assertFalse(iterator.hasNext());
		assertNull(iterator.next());
	}

	@Test
	void singleWordTrie() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		doubleArrayTrie.add("bachelor");
		Iterator<String> iterator = doubleArrayTrie.iterator();
		assertTrue(iterator.hasNext());
		assertEquals("bachelor", iterator.next());
		assertFalse(iterator.hasNext());
	}

	@Test
	void twoWordTrie() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		doubleArrayTrie.add("bachelor");
		doubleArrayTrie.add("jar");
		Iterator<String> iterator = doubleArrayTrie.iterator();
		assertTrue(iterator.hasNext());
		assertEquals("bachelor", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("jar", iterator.next());
		assertFalse(iterator.hasNext());
	}

	@Test
	void threeWordTrie() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		doubleArrayTrie.add("bachelor");
		doubleArrayTrie.add("jar");
		doubleArrayTrie.add("badge");
		Iterator<String> iterator = doubleArrayTrie.iterator();
		assertTrue(iterator.hasNext());
		assertEquals("bachelor", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("badge", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("jar", iterator.next());
		assertFalse(iterator.hasNext());
	}

	@Test
	void withPrefix() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		doubleArrayTrie.add("bachelor");
		doubleArrayTrie.add("jar");
		doubleArrayTrie.add("badge");
		Iterator<String> iteratorBa = doubleArrayTrie.iterator("ba");
		assertTrue(iteratorBa.hasNext());
		assertEquals("bachelor", iteratorBa.next());
		assertTrue(iteratorBa.hasNext());
		assertEquals("badge", iteratorBa.next());
		assertFalse(iteratorBa.hasNext());

		Iterator<String> iteratorJa = doubleArrayTrie.iterator("ja");
		assertTrue(iteratorJa.hasNext());
		assertEquals("jar", iteratorJa.next());
		assertFalse(iteratorJa.hasNext());

		Iterator<String> iteratorK = doubleArrayTrie.iterator("k");
		assertFalse(iteratorK.hasNext());
	}

	@Test
	void fourWordTrie() {
		DoubleArrayTrie doubleArrayTrie = new DoubleArrayTrie();
		doubleArrayTrie.add("bachelor");
		doubleArrayTrie.add("jar");
		doubleArrayTrie.add("badge");
		doubleArrayTrie.add("baby");
		Iterator<String> iterator = doubleArrayTrie.iterator();
		assertTrue(iterator.hasNext());
		assertEquals("baby", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("bachelor", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("badge", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("jar", iterator.next());
		assertFalse(iterator.hasNext());
	}
}
