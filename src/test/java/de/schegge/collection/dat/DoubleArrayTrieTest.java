package de.schegge.collection.dat;

import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

class DoubleArrayTrieTest {

  @Test
  void emptyTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    assertTrue(dat.isEmpty());
    assertEquals(0, dat.size());
    assertFalse(dat.contains("bachelor"));
  }

  @Test
  void singleWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    assertEquals(1, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void sameWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    assertEquals(1, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    dat.add("bachelor");
    assertEquals(1, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
  }

  @Test
  void twoWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    dat.add("jar");
    assertEquals(2, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertTrue(dat.contains("jar"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void threeWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    assertTrue(dat.contains("bachelor"));
    dat.add("jar");
    assertTrue(dat.contains("jar"));
    dat.add("badge");
    assertEquals(3, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertTrue(dat.contains("jar"));
    assertTrue(dat.contains("badge"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void fourWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    dat.add("jar");
    dat.add("badge");
    dat.add("baby");
    assertEquals(4, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertTrue(dat.contains("jar"));
    assertTrue(dat.contains("badge"));
    assertTrue(dat.contains("baby"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void addAll() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertTrue(dat.contains("jar"));
    assertTrue(dat.contains("badge"));
    assertTrue(dat.contains("baby"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void containsAll() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.containsAll(asList("bachelor", "jar", "badge", "baby")));
    assertFalse(dat.containsAll(asList("bachelor", "jar", "badge", "bank")));
  }

  @Test
  void clear() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    assertFalse(dat.isEmpty());
    dat.clear();
    assertEquals(0, dat.size());
    assertTrue(dat.isEmpty());
  }

  @Test
  void addSame() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    dat.add("badge");
    assertFalse(dat.add("badge"));
    assertEquals(4, dat.size());
  }

  @Test
  void fourAndOneWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    dat.add("jar");
    dat.add("ghost");
    dat.add("badge");
    dat.add("baby");
    assertEquals(5, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("bachelor"));
    assertTrue(dat.contains("jar"));
    assertTrue(dat.contains("ghost"));
    assertTrue(dat.contains("badge"));
    assertTrue(dat.contains("baby"));
    assertFalse(dat.contains("test"));
    assertFalse(dat.contains("bank"));
  }

  @Test
  void twoLongWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("abc");
    dat.add("abd");
    assertEquals(2, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("abc"));
    assertTrue(dat.contains("abd"));
  }

  @Test
  void twoLongerWordTrie() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("abcd");
    dat.add("abce");
    assertEquals(2, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.contains("abcd"));
    assertTrue(dat.contains("abce"));
  }

  @Test
  void listWords() throws IOException {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/words")))) {
      reader.lines().map(String::toLowerCase).forEach(dat::add);
      assertEquals(999, dat.size());
    }
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/words")))) {
      reader.lines().map(String::toLowerCase).forEach(word -> assertTrue(dat.contains(word)));
    }
  }

  @Test
  void listDict() throws IOException {
    Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜé").build();
    DoubleArrayTrie dat = new DoubleArrayTrie(alphabet);
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/de_DE-only.dic"), ISO_8859_1))) {
      reader.lines().map(String::trim).forEach(dat::add);
    }

    assertEquals(32634, dat.size());

    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/de_DE-only.dic"), ISO_8859_1))) {
      reader.lines().map(String::trim).forEach(word -> assertTrue(dat.contains(word)));
    }
  }

  @Test
  void listGermanTest() throws IOException {
    Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜéáóèÅëçîåíûñúâàôêÉæøãòïì").build();
    DoubleArrayTrie dat = new DoubleArrayTrie(alphabet);
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/german.txt"), UTF_8))) {
      long start = System.currentTimeMillis();
      reader.lines().forEach(dat::add);
      System.out.println(System.currentTimeMillis() - start);
    }
    assertEquals(1999911, dat.size());
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/german.txt"), UTF_8))) {
      long start = System.currentTimeMillis();
      reader.lines().forEach(word -> assertTrue(dat.contains(word), word));
      System.out.println(System.currentTimeMillis() - start);
    }
  }

  @Test
  void containsPrefix() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.add("bachelor");
    dat.add("jar");
    dat.add("badge");
    dat.add("baby");
    assertEquals(4, dat.size());
    assertFalse(dat.isEmpty());
    assertTrue(dat.containsPrefix("ba"));
    assertTrue(dat.containsPrefix("bachelor"));
    assertTrue(dat.containsPrefix("j"));
    assertTrue(dat.containsPrefix("bad"));
    assertFalse(dat.containsPrefix("ball"));
  }

  @Test
  void containsWithoutString() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    assertFalse(dat.contains(12));
    assertFalse(dat.contains(null));
  }

  @Test
  void stream() throws IOException {
    int max = 330000;
    Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜéáóèÅëçîåíûñúâàôêÉæøã").build();
    DoubleArrayTrie dat = new DoubleArrayTrie(alphabet);
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/german.txt"), UTF_8))) {
      long start = System.currentTimeMillis();
      reader.lines().limit(max).forEach(dat::add);
      System.out.println(System.currentTimeMillis() - start);
    }
    assertEquals(max, dat.size());
    System.out.println(dat.stream().limit(1000).collect(Collectors.joining(", ")));
  }

  @Test
  void streamWithPrefix() throws IOException {
    int max = 330000;
    Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜéáóèÅëçîåíûñúâàôêÉæøã").build();
    DoubleArrayTrie dat = new DoubleArrayTrie(alphabet);
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/german.txt"), UTF_8))) {
      reader.lines().limit(max).forEach(dat::add);
    }
    assertEquals(
        asList("Aaseeterrassen", "Aaseeterrassenfest", "Aaseeterrassenfeste", "Aaseeterrassenfesten",
            "Aaseeterrassenfestes", "Aaseeterrassenfests"),
        dat.stream("Aaseeterrassen").collect(Collectors.toList()));
  }

  @Test
  void iteratorWithPrefix() throws IOException {
    int max = 33000;
    Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜéáóèÅëçîåíûñúâàôêÉæøã").build();
    DoubleArrayTrie dat = new DoubleArrayTrie(alphabet);
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/german.txt"), UTF_8))) {
      reader.lines().limit(max).forEach(dat::add);
    }
    long count = 0;
    Iterator<String> iterator = dat.iterator("Aaseeterrassen");
    while (iterator.hasNext()) {
      iterator.next();
      count++;
    }
    assertEquals(6, count);
  }

  @Test
  void removeTailNode() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    dat.remove("jar");
    assertFalse(dat.contains("jar"));
    assertEquals(3, dat.size());
  }

  @Test
  void removeInnerNodes() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    dat.remove("baby");
    assertFalse(dat.contains("baby"));
    assertEquals(3, dat.size());
    dat.remove("badge");
    assertFalse(dat.contains("badge"));
    assertEquals(2, dat.size());
    dat.remove("bachelor");
    assertFalse(dat.contains("bachelor"));
    assertEquals(1, dat.size());
  }

  @Test
  void removeAll() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    dat.removeAll(asList("bachelor", "jar", "badge", "baby"));
    assertTrue(dat.isEmpty());
  }

  @Test
  void retainAll() {
    DoubleArrayTrie dat = new DoubleArrayTrie();
    dat.addAll(asList("bachelor", "jar", "badge", "baby"));
    assertEquals(4, dat.size());
    dat.retainAll(asList("bachelor", "jar", "bank"));
    assertTrue(dat.containsAll(asList("bachelor", "jar")));
    assertFalse(dat.contains("badge"));
    assertFalse(dat.contains("baby"));
  }
}
