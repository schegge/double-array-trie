package de.schegge.collection.dat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class AlphabetTest {

  @Test
  void invalidAlphabet() {
    Executable executable = () -> Alphabet.with().letters("ab\u0004c").build();
    assertThrows(IllegalArgumentException.class, executable);
  }

  @Test
  void validAlphabet() {
    Alphabet alphabet = Alphabet.with().letters("abcd").build();
    assertEquals('a', alphabet.alpha(2));
    assertEquals(3, alphabet.code('b'));
    assertEquals(5, alphabet.size());
    assertEquals("abc", alphabet.check("abc"));
    assertThrows(IllegalArgumentException.class, () -> alphabet.check("cde"));
    assertEquals("abc\u0004", alphabet.checkEot("abc"));
    assertThrows(IllegalArgumentException.class, () -> alphabet.checkEot("cde"));
  }
}
