package de.schegge.collection;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import de.schegge.collection.dat.DoubleArrayTrie;
import de.schegge.collection.dat.Trie;
import de.schegge.collection.dat.Tries;
import org.junit.jupiter.api.Test;

import java.util.List;

class TriesTest {

  @Test
  void unmodifiableTrie() {
    Trie trie = new DoubleArrayTrie();
    trie.add("bachelor");
    trie.add("jar");
    final Trie unmodifiable = Tries.unmodifiable(trie);
    assertEquals(2, unmodifiable.size());
    assertFalse(unmodifiable.isEmpty());
    assertTrue(unmodifiable.containsPrefix("ba"));
    assertTrue(unmodifiable.prefixed("bachelorette"));
    assertTrue(unmodifiable.contains("bachelor"));
    List<String> list = asList("bachelor", "jar");
    assertTrue(unmodifiable.containsAll(list));
    assertNotNull(unmodifiable.iterator());
    assertEquals(list, asList(unmodifiable.toArray()));
    assertEquals(list, asList(unmodifiable.toArray(new String[0])));
    assertThrows(UnsupportedOperationException.class, () -> unmodifiable.add("badge"));
    assertThrows(UnsupportedOperationException.class, () -> unmodifiable.remove("badge"));
    assertThrows(UnsupportedOperationException.class, () -> unmodifiable.addAll(list));
    assertThrows(UnsupportedOperationException.class, () -> unmodifiable.removeAll(list));
    assertThrows(UnsupportedOperationException.class, () -> unmodifiable.retainAll(list));
    assertThrows(UnsupportedOperationException.class, unmodifiable::clear);
  }

  @Test
  void synchronizedTrie() {
    Trie trie = new DoubleArrayTrie();
    trie.add("bachelor");
    trie.add("jar");
    final Trie synchronizedTrie = Tries.synchronizedTrie(trie);
    assertEquals(2, synchronizedTrie.size());
    assertFalse(synchronizedTrie.isEmpty());
    assertTrue(synchronizedTrie.containsPrefix("ba"));
    assertTrue(synchronizedTrie.prefixed("bachelorette"));
    assertTrue(synchronizedTrie.contains("bachelor"));
    assertTrue(synchronizedTrie.containsAll(asList("bachelor", "jar")));
    assertNotNull(synchronizedTrie.iterator());
    assertEquals(asList("bachelor", "jar"), asList(synchronizedTrie.toArray()));
    assertEquals(asList("bachelor", "jar"), asList(synchronizedTrie.toArray(new String[0])));
    synchronizedTrie.add("badge");
    synchronizedTrie.remove("badge");
    synchronizedTrie.addAll(asList("bachelor", "jar", "baby"));
    synchronizedTrie.removeAll(asList("bachelor", "jar"));
    synchronizedTrie.retainAll(asList("bachelor", "jar", "baby"));
    assertTrue(synchronizedTrie.contains("baby"));
    assertTrue(trie.contains("baby"));
    synchronizedTrie.clear();
    assertFalse(synchronizedTrie.contains("baby"));
    assertFalse(trie.contains("baby"));
  }
}