# Double Array Trie

A Java Implementation for a *Double Array Trie*.
This code is part of the blog articles
[Double Array Tries](https://schegge.de/2018/04/24/double-array-tries/), 
[(Teil 2)](https://schegge.de/2019/11/13/double-array-tries-teil-2/)
and 
[(Teil 3)](https://schegge.de/2019/11/17/double-array-tries-teil-3/)
.

A trie is an efficient data structure to store and retrieve `String` values. 
A word of length $`n`$ can be found in the trie in $`O(n)`$.

The classes in this implementation are not thread-save and concurrent write access ist not permitted.

## Trie

The `Trie` interface conforms to the Java Collection API.
The interface is implemented from the class `DoubleArrayTrie`.

> **Example Trie**
> ```java
> Trie trie = new DoubleArrayTrie();
> trie.add("bachelor");
> trie.addAll(asList("jar", "badge", "baby");
> trie.containsAll(asList("bachelor", "jar", "badge", "baby");
> trie.contains("bachelor");
> ```

## Set

Because the `Trie` interface extends the `Set`interface, the `DoubleArrayTrie` class can also be used as a set.

> **Example Set**
> ```java
> Set<String> set = new DoubleArrayTrie();
> set.add("bachelor");
> set.addAll(asList("jar", "badge", "baby");
> set.containsAll(asList("bachelor", "jar", "badge", "baby");
> set.contains("bachelor");
> ```

## Map
 
The class `DoubleArrayMap` implements the `Map` interface and can be used to handle maps with `String` keys.
Only store and retrieval is supported, existing entries cannot be removed from the trie. 
Because of that, the `remove` methods throws an `UnsupportedOperationException`. 
In addition the `entrySet()`, `keySet()`and `values()` methods return unmodifiable collections. 

> **Example Map**
> ```java
> Map<String, Integer> map = new DoubleArrayMap<Integer>();
> map.put("bachelor", 1);
> map.put("jar", 2);
> map.put("badge", 3);
> map.put("baby", 4);
> List<String> pairs = map.entrySet().stream()
>   .map(e -> e.getKey() + "=" + e.getValue())
>   .collect(toList()));
> ```

## Alphabet

The character set of the stored `Strings` in `DoubleArrayTrie` and `DoubleArrayMap` is restricted.
The default constructors use a character set of characters from *a-z* and *A-Z*.

Other character sets can be specified via a String parameter.

> ```java
> // agtc
> Trie trie = new DoubleArrayTrie("agtc");
> ```

Larger character sets can be constructed via an `Alphabet.Builder`

> ```java
> // 0123456789aAäÄbBcCdDeEfFgGhHiIjJkKlLmMnNoOöÖpPqQrRsSßtTuUüÜvVwWxXyYzZ
> Alphabet alphabet = builder.letters().letters("äöüÄÖÜß").digits().build(GERMAN);
> Trie trie = new DoubleArrayTrie(alphabet);
>
> // abcdefghijklmnopqrstuvwxyz0123456789
> Alphabet alphabet = builder.letters().lowercase.digits().build();
> Trie trie = new DoubleArrayTrie(alphabet);
> ```

## Further Examples

> **Trie as Iterable**
> ```java
> Trie trie = new DoubleArrayTrie();
> trie.addAll(Arrays.asList("bachelor", "jar", "badge", "baby");
> for (String word : trie) {
>   System.out.println(word.toUpperCase());   
> }
> ```

> **Trie as Stream**
> ```java
> Trie trie = new DoubleArrayTrie();
> trie.addAll(Arrays.asList("bachelor", "jar", "badge", "baby");
> trie.stream().map(String::toUpperCase).forEach(System.out::println);
> ```

> **Count words in the *Project Gutenberg* version of Kafkas 'Die Verwandlung'**
> ```java
> @Test
> final Alphabet alphabet = Alphabet.with().letters().letters("äöüßÄÖÜ-").build(GERMANY);
>   Map<String, AtomicInteger> map = new DoubleArrayMap<>(alphabet);
>   try (Scanner s = new Scanner(getClass().getResourceAsStream("/dieverwandlung.txt"))) {
>     while (s.hasNext()) {
>       final String word = s.next().replaceAll("[.,;:!?»«]", "");
>       if (word.matches("^[a-zA-ZaäöüßÄÖÜ-]+$")) {
>         map.computeIfAbsent(word, k -> new AtomicInteger()).getAndIncrement();
>       }
>     }
>   }
>   String result = map.entrySet().stream()
>     .sorted(reverseOrder(comparingInt(a -> a.getValue().get())))
>     .map(e -> e.getKey() + "=" + e.getValue())
>     .collect(joining(", "));
> }
> ```
